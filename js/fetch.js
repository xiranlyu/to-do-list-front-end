function getAllItems(theStatus) {
    fetch('http://localhost:8080/items')
    .then(function(response) {
        return response.json();
    })
    .then(function(items) {
        showItems(items, theStatus);
    })
}

function addItem(newItem) {
    fetch('http://localhost:8080/items', { 
        method: 'POST',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(newItem)
    })
    .then(function(response) {
        return response.json();
    })
    .then(() => getAllItems(theStatus));
}

function deleteItem(id) {
    fetch('http://localhost:8080/items/' + id, { 
        method: 'DELETE',
        headers: {'Content-Type': 'application/json'}
    })
    .then(() => getAllItems(theStatus));
}

function deleteItem() {
    fetch('http://localhost:8080/items', { 
        method: 'DELETE',
        headers: {'Content-Type': 'application/json'}
    })
    .then(() => getAllItems(theStatus));
}

function updateItem(item) {
    fetch('http://localhost:8080/items', { 
        method: 'PUT',
        headers: {'Content-Type': 'application/json'},
        body: JSON.stringify(item)
    })
    .then(function(response) {
        return response.json();
    })
    .then(() => getAllItems(theStatus));
}
let theStatus = 'ALL';

function showItems(items, status) {
    theStatus = status;
    if (theStatus === 'ALL' && items !== undefined) {
        let list = items.map(item => showItem(item))
        .reduce((a, b) => a + b, '');
        document.getElementById('list').innerHTML = list;
    } else if (theStatus !== 'ALL' && items !== undefined) {
        let list = items.filter(item => item.status === status)
        .map(item => showItem(item))
        .reduce((a, b) => a + b, '');
        document.getElementById('list').innerHTML = list;
    }
}

function showItem(item) {
    return '<li id="' + item.id +'"><input type="checkbox" ' + 
    (item.status === 'COMPLETED' ? 'checked' : '') + 
    ' onclick="toggle(' + 
    item.id + ', \'' + item.text + '\', \'' + item.status +
    '\')"/><span id="' +
    item.id +
    '" onclick="editable(this)" onkeydown="editItem(this, ' + '\'' + item.status + '\'' + 
         ')">' + 
    item.text + '</span><button class="deleteButton" onclick="deleteIt(' + item.id +')"></button></li>'
}

function addNewItem() {
    let input = document.getElementById("inputBox").value;
    if (input === '') {
        alert("You didn't type anything!");
    } else {
        let newItem = {
        text: input
        }
        addItem(newItem);
    }
    document.getElementById("inputBox").value = '';
}

function toggle(theId, theText, theStatus) {
    let updatedItem = 
        {
            id: theId,
            text: theText,
            status: (theStatus === 'COMPLETED' ? 'ACTIVE' : 'COMPLETED')
        };
    updateItem(updatedItem);
}

function deleteIt(id) {
    deleteItem(id);
}

function enterItem(key) {
    if (key.keyCode === 13) {
        addNewItem();
    }
}

function clearAllItems() {
    deleteItem();
}

function editable(sp) {
    sp.setAttribute('contenteditable', true);
}

function editItem(sp, theStatus) {
    if (event.keyCode === 13) {
        const thisId = sp.getAttribute('id');
        const newText = sp.innerText;
        sp.setAttribute('contenteditable', false);
        let updatedItem = 
            {
                id: thisId,
                text: newText,
                status: theStatus
            };
        updateItem(updatedItem);
    }
}

function buttonPressed(dom, status) {
    theStatus = status;
    let elements = dom.parentNode.children;
    Array.from(elements).forEach((element) => element.classList.remove('buttonPressed'));
    dom.classList.add('buttonPressed');
    getAllItems(status);
}
